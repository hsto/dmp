**Source Data**

This repository holds source data from our scientific studies published in the DMP-product line. 

---

## Contents

The directories in this repository have the following contents:

* readme.md - this file
* data - publishable study data
* paper - the papers to be reviewed or published 
* slides - presentations to go with the papers

---

## Studies

This repository provides data for all publications of a research project. Currently, the following studies are published or submitted for peer review.

* DMP-1a - a Systematic Mapping Study


---

## Copyright

The author retains full and unrestricted copyright to all of these data. They are not, in any sense, public domain. The only usage for which they are available is the scientific peer review of the above mentioned publications. 

If you wish to use these data for other purposes, such as replicating, checking, or continuing the research, please contact the owner by email, and we will figure it out.